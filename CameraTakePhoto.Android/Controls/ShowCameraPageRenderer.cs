﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Graphics;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Hardware;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using CameraTakePhoto.Controls;
using CameraTakePhoto.Droid.Controls;

[assembly: Xamarin.Forms.ExportRenderer(typeof(ShowCameraPage), typeof(ShowCameraPageRenderer))]
namespace CameraTakePhoto.Droid.Controls
{
    public class ShowCameraPageRenderer : PageRenderer, TextureView.ISurfaceTextureListener
    {
        Android.Hardware.Camera camera;
        CameraFacing cameraType;
        global::Android.Views.View view;
        Activity activity;
        TextureView textureView;

        public ShowCameraPageRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                return;
            }

            try
            {
                SetupUserInterface();
                AddView(view);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(@"			ERROR: ", ex.Message);
            }
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            if (!changed)
                return;

            var msw = MeasureSpec.MakeMeasureSpec(r - l, MeasureSpecMode.Exactly);
            var msh = MeasureSpec.MakeMeasureSpec(b - t, MeasureSpecMode.Exactly);

            view.Measure(msw, msh);
            view.Layout(0, 0, r - l, b - t);
        }

        private void SetupUserInterface()
        {
            activity = this.Context as Activity;

            view = activity.LayoutInflater.Inflate(Resource.Layout.ShowCameraLayout, this, false);

            cameraType = CameraFacing.Back;

            textureView = view.FindViewById<TextureView>(Resource.Id.textureView);
            textureView.SurfaceTextureListener = this;
        }

        #region TextureView.ISurfaceTextureListener implementations
        public void OnSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
        {
            camera = Android.Hardware.Camera.Open((int)cameraType);

            var parameters = camera.GetParameters();
            var aspect = ((decimal)height) / ((decimal)width);

            // Find the preview aspect ratio that is closest to the surface aspect
            var previewSize = parameters.SupportedPreviewSizes
                                        .OrderBy(s => Math.Abs(s.Width / (decimal)s.Height - aspect))
                                        .First();

            System.Diagnostics.Debug.WriteLine($"Preview sizes: {parameters.SupportedPreviewSizes.Count}");

            parameters.SetPreviewSize(previewSize.Width, previewSize.Height);

            //IList<string> focusModes = parameters.SupportedFocusModes;


            parameters.FocusMode = Android.Hardware.Camera.Parameters.FocusModeContinuousPicture;
            camera.SetParameters(parameters);

            camera.SetPreviewTexture(surface);
            camera.SetDisplayOrientation(90);
            camera.StartPreview();
        }

        public bool OnSurfaceTextureDestroyed(SurfaceTexture surface)
        {
            camera.StopPreview();
            camera.Release();

            return true;
        }

        public void OnSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
        {
        }

        public void OnSurfaceTextureUpdated(SurfaceTexture surface)
        {
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CameraTakePhoto.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class APICamera2Page : ContentPage
    {
        public static event EventHandler<ImageSource> PhotoCapturedEvent;

        public APICamera2Page()
        {
            InitializeComponent();

            PhotoCapturedEvent += (sender, source) =>
            {
                PhotoCaptured.Source = source;
            };
        }

        public static void OnPhotoCaptured(ImageSource src)
        {
            PhotoCapturedEvent?.Invoke(new MainPage(), src);
        }
    }
}
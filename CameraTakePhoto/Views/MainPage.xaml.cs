﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Xamarin.Forms;
using Xamarin.Essentials;

using CameraTakePhoto.Controls;

namespace CameraTakePhoto.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void ButtonShowCameraClicked(object sender, EventArgs e)
        {
            if (await getPermissionCamera() != PermissionStatus.Granted)
            {
                await DisplayAlert("Error", "Debe conceder permisos para acceder a la cámara", "OK");
                return;
            }

            var cameraPage = new ShowCameraPage();
            await Navigation.PushModalAsync(cameraPage);
        }

        async void CameraPage_OnPhotoResult(PhotoResultEventArgs result)
        {
            await Navigation.PopModalAsync();
            if (!result.Success)
                return;

            Photo.Source = ImageSource.FromStream(() => new MemoryStream(result.Image));
        }

        private async Task<PermissionStatus> getPermissionCamera()
        {
            var status = await Permissions.CheckStatusAsync<Permissions.Camera>();

            if (status != PermissionStatus.Granted)
            {
                status = await Permissions.RequestAsync<Permissions.Camera>();
            }

            return status;
        }

        private async void Button1Clicked(object sender, System.EventArgs e)
        {
            if (await getPermissionCamera() != PermissionStatus.Granted)
            {
                await DisplayAlert("Error", "Debe conceder permisos para acceder a la cámara", "OK");
                return;
            }

            var cameraPage = new CameraPage();
            cameraPage.OnPhotoResult += CameraPage_OnPhotoResult;
            await Navigation.PushModalAsync(cameraPage);
        }

        private async void Button2Clicked(object sender, EventArgs e)
        {
            if (await getPermissionCamera() != PermissionStatus.Granted)
            {
                await DisplayAlert("Error", "Debe conceder permisos para acceder a la cámara", "OK");
                return;
            }

            var cameraPage = new CameraPage2();
            cameraPage.OnPhotoResult += CameraPage_OnPhotoResult;
            await Navigation.PushModalAsync(cameraPage);
        }

        private async void Button3Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new APICamera2Page());
        }
    }
}
